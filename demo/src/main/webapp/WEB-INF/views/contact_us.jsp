<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="./resources/css/style.css">

<%@ include file="footer.jsp" %>
<div class="container-fluid"> 

	<div class="row">
		<div class="col-sm-2">
			<img src="./resources/images/12.png"/>
		<h5 style="color:deeppink">Your career path begins here</h5>
		</div>

		<div class="col-sm-10"> 
			<div class="col-sm-12"><h1 class="header">DAC SHALA</h1></div>
		</div>
	</div>
	
	
	
	<div class="row " style="margin-top:0.5in">
		<div class="col-sm-10"></div>
		
		<div class="col-sm-1 text-success padding-left:10%">
			<a href="register.jsp" style="color:yellow;font-size:20px">HOME</a>
		</div>
	
		<div class="container ">
	
	<div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="well well-sm">
          <form class="form-horizontal" action="" method="post">
          <fieldset>
            <legend class="text-center">Contact us</legend>
    
            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Name</label>
              <div class="col-md-9">
                <input id="name" name="name" type="text" placeholder="Your name" class="form-control">
              </div>
            </div>
    
            <!-- Email input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="email">Your E-mail</label>
              <div class="col-md-9">
                <input id="email" name="email" type="text" placeholder="Your email" class="form-control">
              </div>
            </div>
    
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="message">Your message</label>
              <div class="col-md-9">
                <textarea class="form-control" id="message" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
              </div>
            </div>
    
            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-primary btn-lg">Submit</button>
              </div>
            </div>
          </fieldset>
          </form>
        </div>
      </div>
	</div>
</div>
		
	</div>	
</div>
