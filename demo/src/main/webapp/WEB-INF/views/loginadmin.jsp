<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<%@ include file="header.jsp" %>
<%@ include file="footer.jsp" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>LOGIN</title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="./resources/css/style.css">
  <script src="./resources/js/loginvalid.js"></script>
</head>

<body>

<div class="container" style="margin-top: 1%;">

  <div class="row">
    <div class="col-sm-4"> </div>
	
	<div class="col-md-4">
  
	<h1 class="text-center"style="color:yellow">SIGN IN</h1>
<br/>

	<div class="col-sm-12">

	<ul class="nav">

		<li class="" style="width:50%"><a class="btn btn-lg btn-default" data-toggle="tab" href="loginadmin">ADMIN</a></li>
   
		<li class="" style="width:48%"><a class=" btn btn-lg btn-default" data-toggle="tab" href="loginstudent">STUDENT</a></li>

	</ul>

<br/>


	<div class="tab-content">
      
	<form action="#">
	
	<div class="form-group nav">
		<label for="UserName">Admin Id</label><p id="errorfield" style="color:red;Font-size:14"></p>
		<input type="text" class="form-control" id="userid">
	</div>

	<div class="form-group nav">

		<label for="pwd">Password:</label><p id="pfield" style="color:red;Font-size:14"></p>
		<input type="password" class="form-control" id="pwd">
	</div>
 

	<button type="button" onclick="lvalid()" class="btn btn-default btn-lg" style="margin-top:20px;">Submit</button>
	<button type="submit" class=" pull-right btn-link" ><a href="www.google.com" style="color:yellow;font-size:25px">Forget password</a></button>

	</form>
	
	</div>
	
	</div>
	
	</div>
	</div>
</div>
</body>
</html>
