package com.dac.demo.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.*;
import java.util.List;
import java.util.Scanner;


import com.dac.demo.model.UploadFile;

@Repository
public class FileUploadDAOImpl implements FileUploadDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public FileUploadDAOImpl()
	{
		
	}

	public FileUploadDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	
	
	@Override
	@Transactional
    public void save(UploadFile uploadFile) {
        String sql = "insert into FILES_UPLOAD(FILE_ID, FILE_NAME, FILE_DATA) values (?, ?, ?)";
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection)
                    throws SQLException {
                PreparedStatement ps = connection.prepareStatement(sql.toString(),
                        Statement.RETURN_GENERATED_KEYS);
                ps.setLong(1, uploadFile.getId());
                Reader reader = new StringReader(uploadFile.getFileName());
                ps.setClob(2, reader);
                ByteArrayInputStream inputStream = new ByteArrayInputStream(uploadFile.getData());
                ps.setBlob(3, inputStream);
                return ps;
            }
        }, holder);
        Number key = holder.getKey();
        if (key != null) {
            return ;
        }
        throw new RuntimeException("No generated primary key returned.");
    }
//	public void save(UploadFile uploadFile) 
//	{
	//	sessionFactory.getCurrentSession().save(uploadFile);
//	} 

}
