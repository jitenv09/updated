package com.dac.demo.dao;

import com.dac.demo.model.UploadFile;

public interface FileUploadDAO {
	void save(UploadFile uploadFile);
}
